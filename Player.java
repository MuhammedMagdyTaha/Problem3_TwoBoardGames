package com.company;

class Player {

    private String name;
    private char mark;
    private int winningTimes;
    private char currentGameStatus;

    public Player(){

    }

    public Player(String name, char mark){
        this.name = name;
        this.mark = mark;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getMark() {
        return mark;
    }

    public void setMark(char mark) {
        this.mark = mark;
    }

    public int getWinningTimes() {
        return winningTimes;
    }

    public void setWinningTimes(int winningTimes) {
        this.winningTimes = winningTimes;
    }

    public char getCurrentGameStatus() {
        return currentGameStatus;
    }

    public void setCurrentGameStatus(char currentGameStatus) {
        this.currentGameStatus = currentGameStatus;
    }
}
