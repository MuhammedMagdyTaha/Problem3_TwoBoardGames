package com.company;

abstract class TwoPlayerBoardGames {

public abstract boolean checkIfWin();
public abstract void togglePlayer();
public abstract void startGame();
public abstract char getCurrentPlayerMark();
public abstract void printCurrentBoardPlayers();
}
