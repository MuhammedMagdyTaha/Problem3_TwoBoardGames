package com.company;

import java.util.Scanner;

class TicTacToe extends  TwoPlayerBoardGames {

    private Board ticTacToeBoard = new Board("TicTacToe", 3, 3);
    private Player player1 = new Player("Player1", 'X');
    private Player player2 = new Player("Player2", 'O');
    private int currentPlayer = 1;


    public TicTacToe() {

        Scanner scanner = new Scanner(System.in);
        ticTacToeBoard.setGameName("TicTacToe");
        System.out.println("TicTacToe Board and Players Created ");

        System.out.println("Please enter 1st player name");
        player1.setName(scanner.next());

        System.out.println("Please enter 1st player mark");
        player1.setMark(scanner.next().toCharArray()[0]);

        System.out.println("Please enter 2nd player name");
        player2.setName(scanner.next());

        System.out.println("Please enter 2nd player mark");
        char playerTwoMark = scanner.next().toCharArray()[0];

        while (playerTwoMark == player1.getMark()) {
            System.out.println("Similar to " + player1.getName() + "'s Mark" + ", Please Choose different Mark...");
            playerTwoMark = scanner.next().toCharArray()[0];
        }

        player2.setMark(playerTwoMark);
    }

    //    start winning status
    private boolean checkRowCol(char c1, char c2, char c3) {
        return ((c1 != '-') && (c1 == c2) && (c2 == c3));
    }

    private boolean checkDiagonal() {
        return ((checkRowCol(ticTacToeBoard.getBoardCell(0, 0),
                ticTacToeBoard.getBoardCell(1, 1),
                ticTacToeBoard.getBoardCell(2, 2))) ||
                (checkRowCol(ticTacToeBoard.getBoardCell(0, 2),
                        ticTacToeBoard.getBoardCell(1, 1),
                        ticTacToeBoard.getBoardCell(2, 0))));
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (checkRowCol(ticTacToeBoard.getBoardCell(0, i), ticTacToeBoard.getBoardCell(1, i), ticTacToeBoard.getBoardCell(2, i)))
                return true;
        }
        return false;
    }

    private boolean checkRow() {
        for (int i = 0; i < ticTacToeBoard.getRow(); i++) {
            if (checkRowCol(ticTacToeBoard.getBoardCell(i, 0), ticTacToeBoard.getBoardCell(i, 1), ticTacToeBoard.getBoardCell(i, 2)))
                return true;
        }
        return false;
    }

    @Override
    public boolean checkIfWin() {
        return (checkRow() || checkCol() || checkDiagonal());
    }

    @Override
    public void togglePlayer() {
        if (currentPlayer == 1)
            currentPlayer = 2;
        else if (currentPlayer == 2)
            currentPlayer = 1;
    }

    @Override
    public char getCurrentPlayerMark() {

        if (currentPlayer == 1)
            return player1.getMark();
        else
            return player2.getMark();
    }

    @Override
    public void printCurrentBoardPlayers() {

    }

    //   initialize new game

    @Override
    public void startGame() {
        while(!ticTacToeBoard.isBoardFull() && !this.checkIfWin()){

            if(ticTacToeBoard.placeMark(getCurrentPlayerMark())){
                togglePlayer();
                ticTacToeBoard.printCurrentBoard();
            }else
                System.out.println("Try another place");
        }

        if(this.ticTacToeBoard.isBoardFull())
            System.out.println("Board is Full, No one Won");

        if(this.checkIfWin()) {
            if (currentPlayer == 1){
                System.out.println(player1.getName() + " is the Winner..");
            }else if (currentPlayer == 2){
                System.out.println(player2.getName() + " is the Winner..");
            }
        }
    }



}