package com.company;

import java.util.Scanner;

class Board {

     private String gameName;
     private int row;
     private int col;
     private char board[][];


     private void initializeBoard(){
         for (int i = 0 ; i < this.row ; i++)
             for (int j = 0 ; j < this.col ; j++)
                 this.board[i][j] = '-';
     }

     public Board(String gameName, int row, int col){
         this.row = row;
         this.col = col;
         this.gameName = gameName;
         board = new char[this.row][this.col];
         initializeBoard();
     }

     public char getBoardCell(int row, int col){
         if(((col < this.col) && (col >= 0)) && (row < this.col) && (row >= 0)){
             return this.board[row][col];
         }else {

             return 0;
         }
     }

     public void printCurrentBoard(){

         System.out.println("_______");

         for(int i = 0 ; i < this.row ; i++){
             System.out.print("|");
             for(int j = 0 ; j < this.col ; j++){
                 System.out.print(this.board[i][j] + "|");
             }
             System.out.println();
             System.out.println("_______");
         }
     }

     public boolean isBoardFull() {

         for (int i = 0; i < this.getRow(); i++) {
             for (int j = 0; j < this.getCol(); j++) {
                 if(this.board[i][j] == '-')
                      return false;
             }
         }
         return true;
     }

     public boolean placeMark(char playerMark){

         if(this.gameName.trim().equals("ConnectFour")){

             System.out.println("Drop a coin at column (0–6): ");
             Scanner scan = new Scanner(System.in);

             int col =  scan.nextInt();
             if(((col < this.col) && (col >= 0))) {
                 for (int i = 5; i >= 0; i--) {
                     if (board[i][col] == '-') {
                         board[i][col] = playerMark;
                         return true;
                     }
                 }
                return false;
             }
         }else if (this.gameName.trim().equals("TicTacToe")){

             Scanner scan = new Scanner(System.in);
             System.out.println("Please enter the row");
             int row =  scan.nextInt();
             System.out.println("Please enter the col");
             int col = scan.nextInt();
             if( (row < this.row) && (row >=0)  && (col < this.col) && (col >= 0)){
                 if( this.board[row][col] == '-') {
                     this.board[row][col] = playerMark;
                     return true;
                 }else
                     System.out.println("Already used Place.");
             }else
                 System.out.println("Out of range row or col, Please enter valid values...");
         }else
             return false;
         return false;
     }

     public String getGameName() {
         return gameName;
     }

     public void setGameName(String gameName) {
         this.gameName = gameName;
     }

     public int getRow() {
         return row;
     }

     public void setRow(int row) {
         this.row = row;
     }

     public int getCol() {
         return col;
     }

     public void setCol(int col) {
         this.col = col;
     }
 }
