package com.company;

import java.util.Scanner;

class ConnectFour extends TwoPlayerBoardGames{

    private Board connectFourBoard = new Board("Connect Four", 6, 6);
    private Player player1 = new Player();
    private Player player2 = new Player();
    private int currentPlayer = 1;

    public ConnectFour(){

        Scanner scanner = new Scanner(System.in);
        connectFourBoard.setGameName("ConnectFour");
        connectFourBoard.setRow(6);
        connectFourBoard.setCol(6);
        System.out.println("Connect Four Board Created ");

        System.out.println("Please enter 1st player name");
        player1.setName(scanner.next());

        System.out.println("Please enter 2nd player name");
        player2.setName(scanner.next());

        player1.setMark('R');
        player2.setMark('Y');
    }

    @Override
    public boolean checkIfWin() {

         // check horizontal row case

        for (int i = 0 ; i < connectFourBoard.getRow() ; i++){
            for (int j = 0 ; j < connectFourBoard.getCol() ; j++){
                if ((connectFourBoard.getBoardCell(i, j) != '-') &&
                    (connectFourBoard.getBoardCell(i,j+1) != '-') &&
                    (connectFourBoard.getBoardCell(i, j+2) != '-') &&
                    (connectFourBoard.getBoardCell(i, j+3) != '-') &&
                    (connectFourBoard.getBoardCell(i,j) == connectFourBoard.getBoardCell(i,j+1)) &&
                    (connectFourBoard.getBoardCell(i,j+1) == connectFourBoard.getBoardCell(i,j+2)) &&
                    (connectFourBoard.getBoardCell(i,j+2) == connectFourBoard.getBoardCell(i,j+3))){
                        return true;
                }
            }
        }

        //check vertical col case

        for (int i = 0 ; i < connectFourBoard.getCol() ; i++) {
            for (int j = 0 ; j < connectFourBoard.getRow() ; j++){
                if ((connectFourBoard.getBoardCell(j, i) != '-') &&
                        (connectFourBoard.getBoardCell(j+1,i) != '-') &&
                        (connectFourBoard.getBoardCell(j+2, i) != '-') &&
                        (connectFourBoard.getBoardCell(j+3, i) != '-') &&
                        (connectFourBoard.getBoardCell(j, i) == connectFourBoard.getBoardCell(j+1, i)) &&
                        (connectFourBoard.getBoardCell(j+1, i) == connectFourBoard.getBoardCell(j+2, i)) &&
                        (connectFourBoard.getBoardCell(j+2, i) == connectFourBoard.getBoardCell(j+3, i))){
                    return true;
                }
            }
        }

        // check diagonal line case

        for (int i = 0 ; i < connectFourBoard.getCol() ; i++){
            for (int j = 0 ; j < connectFourBoard.getCol() ; j++){
                 if ((connectFourBoard.getBoardCell(i, j) != '-') &&
                         (connectFourBoard.getBoardCell(i+1, j+1) != '-') &&
                         (connectFourBoard.getBoardCell(i+2, j+2) != '-') &&
                         (connectFourBoard.getBoardCell(i+3, j+3) != '-') &&
                         (connectFourBoard.getBoardCell(i, j) == connectFourBoard.getBoardCell(i+1, j+1)) &&
                         (connectFourBoard.getBoardCell(i+1, j+1) == connectFourBoard.getBoardCell(i+2, j+2)) &&
                         (connectFourBoard.getBoardCell(i+3, j+3) == connectFourBoard.getBoardCell(i+3, j+3))){
                     return true;
                 }
             }
         }

       return false;
    }

    @Override
    public void togglePlayer() {
        if (currentPlayer == 1)
            currentPlayer = 2;
        else if (currentPlayer == 2)
            currentPlayer = 1;
    }

    @Override
    public void startGame() {
        while (!connectFourBoard.isBoardFull() && !this.checkIfWin()){

            if(connectFourBoard.placeMark(getCurrentPlayerMark())){
                togglePlayer();
                connectFourBoard.printCurrentBoard();
            }else
                System.out.println("Try another place");
        }

        if(this.connectFourBoard.isBoardFull())
            System.out.println("Board is Full, No one Won");

        if(this.checkIfWin()) {
            if (currentPlayer == 1){
                System.out.println(player1.getName() + " is the Winner..");
            }else if (currentPlayer == 2){
                System.out.println(player2.getName() + " is the Winner..");
            }
        }
    }

    @Override
    public char getCurrentPlayerMark() {
        if (currentPlayer == 1)
            return player1.getMark();
        else if (currentPlayer ==2)
            return player2.getMark();
        else
            return 0;
    }

    @Override
    public void printCurrentBoardPlayers() {
        System.out.println("");
    }


}
