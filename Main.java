package com.company;

import sun.net.ConnectionResetException;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        while(true) {
            System.out.println("Please Select Game to Play (XO , Connect). or Exit ");
            Scanner scanner = new Scanner(System.in);
            String game = scanner.next();

            if (game.trim().equals("XO")) {
                TicTacToe ticTacToe = new TicTacToe();
                ticTacToe.startGame();
            } else if (game.trim().equals("Connect")) {
                ConnectFour connectFour = new ConnectFour();
                connectFour.startGame();
            } else if (game.trim().equals("Exit")){
              System.exit(0);
            } else
                System.out.println("Invalid Input, Please enter a game");
        }
    }
}
